export class AngularAuthPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('angular-auth-app h1')).getText();
  }
}
