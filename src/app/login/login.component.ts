import { Component } from '@angular/core';
import { NgForm }    from '@angular/forms';
import { AngularFire, AuthProviders, AuthMethods } from 'angularfire2';

@Component({
	moduleId: module.id,
	selector: 'app-login',
	templateUrl: 'login.component.html',
	styleUrls: ['login.component.css']
})
export class LoginComponent {

	constructor(public af: AngularFire) {
		this.af.auth.subscribe(auth => console.log(auth));
	}

	public userEmail: string = 'juan@gmail.com';
	public userPassword: any = 'qwerty';
	public hasError: boolean = false;
	public errorMessage: string;

	overrideLogin() {
		this.af.auth.login({
			provider: AuthProviders.Anonymous,
			method: AuthMethods.Anonymous,
		});    
	}

    // Email and password
    emailLogin() {
    	this.af.auth.login(
    	{
    		email: this.userEmail,
    		password: this.userPassword
    	},
    	{
    		provider: AuthProviders.Password,
    		method: AuthMethods.Password,
    	}).then(_ => console.log('success'))
    	.catch(err => this.showError(err));
    }

    logOut(){
    	this.af.auth.logout();
    }

    showError(err: any) {
    	this.hasError = true;
    	this.errorMessage = err;
    }

}
