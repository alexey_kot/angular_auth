import { Component } from '@angular/core';
import { AngularFire } from 'angularfire2';
import { LoginComponent } from './login';

@Component({
  moduleId: module.id,
  selector: 'angular-auth-app',
  templateUrl: 'angular-auth.component.html',
  styleUrls: ['angular-auth.component.css'],
  directives:[LoginComponent]
})
export class AngularAuthAppComponent {
 
}
